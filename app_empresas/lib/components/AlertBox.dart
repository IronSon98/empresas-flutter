import 'package:app_empresas/config/Cores.dart';
import 'package:flutter/material.dart';

alertBox(BuildContext context, String title, String message) {
  showDialog(
    context: context,
    builder: (context) {
      return AlertDialog(
        title: Text(title),
        titleTextStyle: TextStyle(
          fontFamily: 'Heebo',
          fontSize: 20.0,
          fontWeight: FontWeight.w500,
          color: roxa,
        ),
        content: Text(message),
        contentTextStyle: TextStyle(
          fontFamily: 'Heebo',
          fontSize: 16.0,
          fontWeight: FontWeight.w500,
          color: Colors.black,
        ),
        actions: <Widget>[
          ElevatedButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text(
              'Fechar',
              style: TextStyle(
                fontFamily: 'Heebo',
                fontSize: 16.0,
                fontWeight: FontWeight.w500,
              ),
            ),
            style: ElevatedButton.styleFrom(
              primary: roxa,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0),
              ),
            ),
          ),
        ],
      );
    },
  );
}
