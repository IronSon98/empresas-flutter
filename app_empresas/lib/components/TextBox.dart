import 'package:flutter/material.dart';

class TextBox extends StatelessWidget {
  final String label;
  final TextEditingController controller;
  final FormFieldValidator<String> validator;
  final bool? prefixIcon;
  final bool obscureText;

  const TextBox(
      {Key? key,
      required this.label,
      required this.controller,
      required this.validator,
      this.prefixIcon,
      required this.obscureText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      validator: validator,
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10.0),
          ),
        ),
        labelText: label,
        labelStyle: heeboStyle(),
        errorStyle: heeboStyle(),
        prefixIcon: prefixIcon == null ? null : Icon(Icons.search),
      ),
      style: heeboStyle(),
      obscureText: obscureText,
    );
  }

  TextStyle heeboStyle() {
    return TextStyle(
      fontFamily: 'Heebo',
      fontSize: 16.0,
      fontWeight: FontWeight.w500,
    );
  }
}
