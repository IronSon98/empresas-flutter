import 'package:app_empresas/components/AlertBox.dart';
import 'package:app_empresas/components/Button.dart';
import 'package:app_empresas/components/TextBox.dart';
import 'package:app_empresas/screens/HomeScreen.dart';
import 'package:app_empresas/services/LoginService.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:modal_progress_hud/modal_progress_hud.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _controllerEmail = TextEditingController();
  final _controllerSenha = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool _showProgressLoading = false;

  @override
  void initState() {
    super.initState();
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(16.0),
        child: ModalProgressHUD(
          opacity: 0.0,
          inAsyncCall: _showProgressLoading,
          child: Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 50.0),
                  Text(
                    'Boas vindas,',
                    style: TextStyle(
                      fontFamily: 'Gilroy',
                      fontSize: 40.0,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  Text(
                    'Você está no Empresas.',
                    style: TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: 24.0,
                      fontWeight: FontWeight.w100,
                    ),
                  ),
                  SizedBox(height: 55.0),
                  Text(
                    'Digite seus dados para continuar.',
                    style: TextStyle(
                      fontFamily: 'Gilroy',
                      fontSize: 16.0,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  SizedBox(height: 24.0),
                  TextBox(
                    label: 'E-mail',
                    controller: _controllerEmail,
                    validator: _emailValidator,
                    obscureText: false,
                  ),
                  SizedBox(height: 20.0),
                  TextBox(
                    label: 'Senha',
                    controller: _controllerSenha,
                    validator: _passwordValidator,
                    obscureText: true,
                  ),
                  SizedBox(height: 40.0),
                  Button(
                    label: 'ENTRAR',
                    onPressed: () {
                      _clickButton(context);
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  String? _emailValidator(String? email) {
    if (email == null || email.isEmpty) {
      return 'Informe um e-mail.';
    }

    final bool isValid = EmailValidator.validate(email);

    if (!isValid) {
      return 'E-mail inválido!';
    }
    return null;
  }

  String? _passwordValidator(String? password) {
    if (password == null || password.isEmpty) {
      return 'Informe uma senha.';
    }
    return null;
  }

  _clickButton(BuildContext context) async {
    bool formOK = _formKey.currentState!.validate();

    if (!formOK) {
      return;
    }

    String email = _controllerEmail.text;
    String senha = _controllerSenha.text;

    setState(() {
      _showProgressLoading = true;
    });

    var response = await LoginService.login(email, senha);

    new Future.delayed(
      new Duration(seconds: 1),
      () {
        setState(
          () {
            _showProgressLoading = false;
          },
        );
      },
    );

    if (response) {
      _controllerEmail.clear();
      _controllerSenha.clear();
      Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => HomeScreen()),
        (Route<dynamic> route) => false,
      );
    } else {
      alertBox(context, 'Login', 'Credenciais inválidas!');
    }
  }
}
