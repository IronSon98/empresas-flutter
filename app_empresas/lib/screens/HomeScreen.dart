import 'package:app_empresas/components/Button.dart';
import 'package:app_empresas/components/TextBox.dart';
import 'package:app_empresas/config/Cores.dart';
import 'package:app_empresas/screens/DescriptionScreen.dart';
import 'package:app_empresas/services/EnterpriseService.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:modal_progress_hud/modal_progress_hud.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final _controllerName = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  // ignore: unused_field
  bool _searchEnterpriseOk = false;
  bool _showProgressLoading = false;
  int _counter = 0;
  var enterpriseName;
  var photo;
  var description;

  Future<void> _resultSearchOk() async {
    var prefs = await SharedPreferences.getInstance();
    enterpriseName = (prefs.getString('enterprise_name') ?? '');
    photo = (prefs.getString('photo') ?? '');
    description = (prefs.getString('description') ?? '');
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(16.0),
        child: ModalProgressHUD(
          opacity: 0.0,
          inAsyncCall: _showProgressLoading,
          child: Form(
            key: _formKey,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 40.0),
                  Text(
                    'Pesquise por',
                    style: TextStyle(
                      fontFamily: 'Gilroy',
                      fontSize: 36.0,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  Text(
                    'uma empresa',
                    style: TextStyle(
                      fontFamily: 'Gilroy',
                      fontSize: 36.0,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                  SizedBox(height: 16.0),
                  TextBox(
                    label: 'Buscar empresa',
                    controller: _controllerName,
                    validator: _searchValidator,
                    prefixIcon: true,
                    obscureText: false,
                  ),
                  SizedBox(height: 30.0),
                  Button(
                    label: 'Buscar',
                    onPressed: () {
                      _clickButton(context);
                    },
                  ),
                  SizedBox(height: 32.0),
                  _counter == 0
                      ? Container()
                      : Container(
                          child: _searchEnterpriseOk == false
                              ? Center(
                                  child: Column(
                                    children: <Widget>[
                                      Container(
                                        width: 200.0,
                                        height: 200.0,
                                        child: Image.asset(
                                            'lib/assets/imgs/search-failed.png'),
                                      ),
                                      SizedBox(height: 5.0),
                                      Text(
                                        'Empresa não encontrada',
                                        style: TextStyle(
                                          fontFamily: 'Heebo',
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              : Center(
                                  child: Card(
                                    elevation: 10.0,
                                    child: InkWell(
                                      onTap: () {
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) =>
                                                DescriptionScreen(
                                              enterpriseName: enterpriseName,
                                              description: description,
                                              photo: photo,
                                            ),
                                          ),
                                        );
                                      },
                                      child: Container(
                                        height: 200.0,
                                        width: 250.0,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: <Widget>[
                                            Container(
                                              padding: EdgeInsets.all(8.0),
                                              decoration: BoxDecoration(
                                                borderRadius: BorderRadius.all(
                                                  Radius.circular(8.0),
                                                ),
                                              ),
                                              child: ClipRRect(
                                                borderRadius:
                                                    BorderRadius.circular(12.0),
                                                child: Image.network(
                                                  'https://empresas.ioasys.com.br$photo',
                                                ),
                                              ),
                                            ),
                                            SizedBox(height: 8.0),
                                            Divider(
                                              height: 2.0,
                                              thickness: 1.0,
                                              color: roxa,
                                            ),
                                            SizedBox(height: 10.0),
                                            Text(
                                              enterpriseName,
                                              style: TextStyle(
                                                fontFamily: 'Gilroy',
                                                fontSize: 18.0,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                        ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  String? _searchValidator(String? name) {
    if (name == null || name.isEmpty) {
      return 'Informe o nome de uma empresa.';
    }
    return null;
  }

  _clickButton(BuildContext context) async {
    bool formOK = _formKey.currentState!.validate();

    if (!formOK) {
      return;
    }

    String enterpriseName = _controllerName.text;

    setState(() {
      _showProgressLoading = true;
    });

    var response = await EnterpriseService.enterpriseSearch(enterpriseName);

    _controllerName.clear();

    new Future.delayed(
      new Duration(seconds: 1),
      () {
        setState(
          () {
            _showProgressLoading = false;
          },
        );
      },
    );

    if (response) {
      setState(() {
        _counter++;
        _searchEnterpriseOk = true;
        _resultSearchOk();
      });
    } else {
      setState(() {
        _counter++;
        _searchEnterpriseOk = false;
      });
    }
  }
}
