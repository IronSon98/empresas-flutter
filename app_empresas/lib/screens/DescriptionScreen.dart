import 'package:flutter/material.dart';

// ignore: must_be_immutable
class DescriptionScreen extends StatelessWidget {
  var enterpriseName;
  var description;
  var photo;

  DescriptionScreen({
    Key? key,
    required this.description,
    required this.photo,
    required this.enterpriseName,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          enterpriseName,
          style: TextStyle(
            fontFamily: 'Gilroy',
            fontSize: 24.0,
            fontWeight: FontWeight.w700,
          ),
        ),
        centerTitle: true,
        toolbarHeight: 120.0,
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                child: ClipRRect(
                  borderRadius: BorderRadius.only(
                    bottomRight: Radius.circular(12.0),
                    bottomLeft: Radius.circular(12.0),
                  ),
                  child: Image.network(
                    'https://empresas.ioasys.com.br$photo',
                  ),
                ),
              ),
              SizedBox(height: 18.0),
              Container(
                padding: EdgeInsets.all(16.0),
                child: Text(
                  description,
                  style: TextStyle(
                    fontFamily: 'Gilroy',
                    fontSize: 16.0,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
