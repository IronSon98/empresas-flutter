import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class EnterpriseService {
  static Future<bool> enterpriseSearch(String enterpriseName) async {
    String url =
        'https://empresas.ioasys.com.br/api/v1/enterprises?name=$enterpriseName';

    var prefs = await SharedPreferences.getInstance();

    var accessToken = (prefs.getString('access-token') ?? '');
    var client = (prefs.getString('client') ?? '');
    var uid = (prefs.getString('uid') ?? '');

    var header = {
      'Content-Type': 'application/json',
      'access-token': accessToken,
      'client': client,
      'uid': uid,
    };

    http.Response response;

    response = await http.get(Uri.parse(url), headers: header);

    if (response.statusCode == 200) {
      Map mapResponse = json.decode(response.body);
      List<dynamic> listResponse = mapResponse['enterprises'];

      if (listResponse.length > 0) {
        mapResponse = listResponse.first;
      } else {
        return false;
      }

      var enterpriseName = mapResponse['enterprise_name'];
      var photo = mapResponse['photo'];
      var description = mapResponse['description'];

      prefs.setString('enterprise_name', enterpriseName.toString());
      prefs.setString('photo', photo.toString());
      prefs.setString('description', description.toString());

      return true;
    } else {
      return false;
    }
  }
}
