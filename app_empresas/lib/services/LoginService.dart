import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class LoginService {
  static Future<bool> login(String email, String senha) async {
    String url = 'https://empresas.ioasys.com.br/api/v1/users/auth/sign_in';

    var header = {'Content-Type': 'application/json'};

    Map params = {"email": email, "password": senha};

    var prefs = await SharedPreferences.getInstance();

    var _body = json.encode(params);

    http.Response response;

    response = await http.post(Uri.parse(url), headers: header, body: _body);

    var accessToken = response.headers['access-token'];
    var client = response.headers['client'];
    var uid = response.headers['uid'];

    Map mapResponse = json.decode(response.body);

    print(mapResponse);

    if (response.statusCode == 200) {
      prefs.setString('access-token', accessToken.toString());
      prefs.setString('client', client.toString());
      prefs.setString('uid', uid.toString());

      return true;
    } else {
      return false;
    }
  }
}
